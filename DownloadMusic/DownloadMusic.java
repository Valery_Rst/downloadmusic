net.rstvvoli.downloadMusic

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class DownloadMusic {
    private static final String IN_FILE_TXT = "E:\\forproject\\downloadmusic\\inFile.txt";
    private static final String OUT_FILE_TXT = "E:\\forproject\\downloadmusic\\outFile.txt";
    private static final String PATH_TO_MUSIC = "E:\\forproject\\downloadmusic\\music";

    public static void main(String[] args) {

        try (BufferedReader inFile = new BufferedReader(new FileReader(IN_FILE_TXT));
             BufferedWriter outFile = new BufferedWriter(new FileWriter(OUT_FILE_TXT))) {
                extractionOfLinksAndSave(inFile,outFile);
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        try (BufferedReader musicFile = new BufferedReader(new FileReader(OUT_FILE_TXT))) {
            downloadMusic(musicFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод извлекает из вебстраниц ссылки на музыку из потока inFile
     * и сохраняет их в поток outFile;
     * @param inFile файл, из которого извлкчётся ссылка на сайт;
     * @param outFile файл, в который запишутся ссылки на музыку;
     * @throws IOException ошибка ввода-вывода;
     */
    private static void extractionOfLinksAndSave(BufferedReader inFile, BufferedWriter outFile) throws IOException {

        String link;
        while ((link = inFile.readLine()) != null) {
            URL url = new URL(link);
            String result;
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
                result = bufferedReader.lines().collect(Collectors.joining("\n"));
            }
            Pattern musicLinkPattern = Pattern.compile("\\s*(?<=data-url\\s?=\\s?\")[^>]*\\/*(?=\")");
            Matcher matcher = musicLinkPattern.matcher(result);
            int i = 0;
            while (matcher.find() && i < 2) {
                    outFile.write(matcher.group() + "\r\n");
                    i++;
                }
            }
        }

    /**
     * Метод скачивает найденные изображения из файла;
     * @param musicFile файл, из которого необходимо скачать музыку;
     */
    private static void downloadMusic(BufferedReader musicFile) {

        String music;
        int count = 0;
        try {
            while ((music = musicFile.readLine()) != null) {
                downloadUsingNIO(music, PATH_TO_MUSIC + String.valueOf(count) + ".mp3");
                count++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод скачивает музыку из интерента по ссылку strUrl в файл на компьютере file;
     * @param strUrl ссылка на файл в интернете;
     * @param file путь к файлу, в который будет скачано содержимое из интернета;
     * @throws IOException ошибка ввода-вывода;
     */
    private static void downloadUsingNIO(String strUrl, String file) throws IOException {

        URL url = new URL(strUrl);
        /*
         * ReadableByteChannel - канал, способный читать байты из файла;
         */
        ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
        FileOutputStream stream = new FileOutputStream(file);
        /*
         * getChannel() позволяет преобразовать поток вывода stream в файловый канал;
         * transferFrom() позволяет передать данные из byteChannel в файл,
         * к которому привязан fileChannel;
         * 0 - стартовая позиция для чтения;
         * Long.MAX_VALUE - количество прочитанных байт данных;
         * MAX_VALUE - максимальное значение, которое можно хранить в переменной типа long, означает что из источника мы прочитаем все байты;
         */
        stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        System.out.println("Скачивание файла " + file + " прошло успешно");
        stream.close();
        byteChannel.close();
    }
}